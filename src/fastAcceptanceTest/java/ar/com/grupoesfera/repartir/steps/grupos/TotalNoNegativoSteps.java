package ar.com.grupoesfera.repartir.steps.grupos;
import ar.com.grupoesfera.repartir.model.Grupo;
import ar.com.grupoesfera.repartir.steps.FastCucumberSteps;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class TotalNoNegativoSteps extends FastCucumberSteps {
    Grupo grupo;
    String error = "";

    @Dado("un grupo")
    public void un_grupo() {
        grupo = new Grupo();
    }

    @Cuando("el usuario intenta poner un gasto de {double}")
    public void el_usuario_intenta_poner_un_gasto_de(Double total) {
        try {
            grupo.setTotal(new BigDecimal(total));

        } catch (IllegalArgumentException iae) {
            error = iae.getMessage();
        }
    }

    @Entonces("muestra un error")
    public void muestra_un_error() {
        assertThat(error).isEqualTo("No se puede guardar: Total no puede ser negativo");

    }

    @Entonces("el total del grupo es de {double}")
    public void el_total_del_grupo_es_de(Double total) {
        assertThat(grupo.getTotal().compareTo(BigDecimal.valueOf(total))).isEqualTo(0);
    }
}
