# language: es

Característica: Crear Grupo para repartir gastos

  Regla: Los grupos están compuestos por al menos dos miembros

    Escenario: No puedo crear un grupo con un único miembro
      Cuando el usuario intenta crear un grupo indicando un único miembro
      Entonces no debería crear el grupo con un único miembro

  Regla: el total no puede ser negativo

      Escenario: un total de -100 no es válido
        Dado un grupo
        Cuando el usuario intenta poner un gasto de -100
        Entonces muestra un error

      Escenario: un total de 35.5 es válido
        Dado un grupo
        Cuando el usuario intenta poner un gasto de 35.5
        Entonces el total del grupo es de 35.5

      Escenario: un total de 0 es válido
        Dado un grupo
        Cuando el usuario intenta poner un gasto de 0
        Entonces el total del grupo es de 0